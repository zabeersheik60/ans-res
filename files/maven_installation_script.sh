#!/bin/bash

wget https://mirrors.estointernet.in/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
tar -xvf apache-maven-3.6.3-bin.tar.gz
mv apache-maven-3.6.3 maven3
rm -rf apache-maven-3.6.3-bin.tar.gz

sudo echo 'export MAVEN_HOME="/home/ansible/maven3"' >> .bashrc
sudo echo "export PATH=/home/ansible/maven3/bin:$PATH" >> .bashrc


sudo su -
su - ansible
exit

