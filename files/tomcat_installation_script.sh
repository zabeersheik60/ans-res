#TOMCAT INSTALLATION
wget http://apachemirror.wuchna.com/tomcat/tomcat-8/v8.5.54/bin/apache-tomcat-8.5.54.tar.gz
tar -xzvf apache-tomcat-8.5.54.tar.gz
sudo mv apache-tomcat-8.5.54 tomcat8
sudo rm -rf apache-tomcat-8.5.54.tar.gz

sed -i '/className/d' /home/ansible/tomcat8/webapps/manager/META-INF/context.xml
sed -i '/allow/d' /home/ansible/tomcat8/webapps/manager/META-INF/context.xml

#Tomcat-users.xml (Adding User)
cd /home/ansible/tomcat8/conf
sed -i '/version="1.0">/r /home/ansible/tomcat_server_details.txt' /home/ansible/tomcat8/conf/tomcat-users.xml

#Adding Nexus

#cd /home/ansible/tomcat8/webapps

#wget https://gitlab.com/zabeersheik60/java-software/-/raw/master/nexus.war

sudo su -

sh /home/ansible/tomcat8/bin/startup.sh
sh /home/ansible/tomcat8/bin/shutdown.sh
sh /home/ansible/tomcat8/bin/startup.sh

exit
