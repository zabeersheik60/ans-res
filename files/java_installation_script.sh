#!/bin/bash
wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" https://download.oracle.com/otn-pub/java/jdk/8u251-b08/3d5a2bb8f8d4428bbe94aed7ec7ae784/jdk-8u251-linux-x64.tar.gz
tar -xvf jdk-8u251-linux-x64.tar.gz
rm jdk-8u251-linux-x64.tar.gz


sudo echo 'export JAVA_HOME="/home/ansible/jdk1.8.0_251"' >> .bashrc

sudo echo "export PATH=/home/ansible/jdk1.8.0_251/bin:$PATH" >> .bashrc


sudo su -
su - ansible
